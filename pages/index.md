---
title: About the Quarterly Release Pact
slug: index
author: Nils Hilbricht, Christopher Arndt
date: 2020-10-14 21:08:00 UTC+02:00
description:
link:
tags:
---
The release pact is an informal agreement to aim for shared, scheduled software release dates.


# [Where](#where)

To see a listing of new software releases, please go to <https://libreav.org/updates>.


# [When](#when)

We agree to release at least four times a year:

* January, 15th
* April, 15th
* July, 15th
* and October, 15th.

A release is an important step in the development and life of software. Users look forward to
updates and improvements, but they mean additional work for developers. It is often very hard to
decide if and when to release, so developers tend to wait and postpone. There do not seem to be any
objective, measurable reasons that could lead to a decision. Therefore we have decided to use time
as a basis.


# [Why](#why)

**Developer side:**

* Incentive to release something.  Releases are better than git progress. They get packaged, they
indicate a (relatively) good state of the program.
* Momentum/Peer Pressure: Other people are going to release, so will I.
* Healthy, Active Community:  Being in a developer group that you see working (by their releases)
is a good motivation to do something yourself.

**User side:**

* Announcements: Keep the software in the public eye
* Trust. People see that the software is in development and is cared for.
* The "last updated" date should never be more than 4 months away and always the current year.
* Swarm Marketing: A small release does not have much impact and won't get featured often by news
sites. A whole group of software releases demands more attention.


# [Minimum Release](#minimum-release)

"Fixed typo in documentation" (exaggeration) should be enough. Especially for software that has
huge release intervals, like a year or longer, there is public uncertainty if a project is just
"working as intended" or dead.  A minor release with minimal changes is still a signal to the
public that the software is not forgotten.


# [How](#how)

As a developer, to get your project listed on libreav.org, just *release* your software on one of
the dates given above (we allow for a 10 day window centered on those dates) and then *announce*
the release on one of the usual channels, i.e the Linux Audio
[Announce](https://lists.linuxaudio.org/listinfo/linux-audio-user),
[User](https://lists.linuxaudio.org/listinfo/linux-audio-user) or
[Developer](https://lists.linuxaudio.org/listinfo/linux-audio-dev) mailing lists,
[linuxmusicians.com](https:://linuxmusicians.com) or on the **`#lad`** or **`#lau`** channels on
IRC ([freenode](https://freenode.net/)). You can also add your software by going to
<https://libreav.org/add/software> and filling out the form. After approval, the site admin will
add your software to the list of projects, which are regularly checked for updates automatically.
It greatly helps, if your projects publishes its releases on Github or Gitlab and has proper
release notes in the release description.
