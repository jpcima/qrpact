# Quarterly Release Pact Web Site

This repository contains the source used to build the
[Quarterly Release Pact] web site using the [Nikola] static
site generator.


## Build Instructions

The configuration file for the site is `conf.py`.

To build the site:

    $ virtualenv -p python3 venv
    $ source venv/bin/activate
    (venv)$ python -m pip install -r requirements.txt
    (venv)$ nikola build

To test it locally:

    (venv)$ nikola serve -b

To deploy it (SSH key needs to be installed on the target server):

    (venv)$ nikola deploy

To check all available commands:

    (venv)$ nikola help


## Adding a Release

Add a new post to the site:

    (venv)$ nikola new_post -t "thingamob 1.0.1" -e

This will open your default editor with the Markdown source of the new post.

The top of the file contains the post's meta data (between the `---` lines).
Change the URLs of the `link`, `homepage` and `repository` tags to be
appropriate for your project's release. The URL for the `link` tag should lead
to a page with the release notes or a change log for your project.

If your project doesn't have online release notes or a repository, just set the
respective tag to an empty string (`''`).

Also set the `author` and `date` tags as you see fit. Ignore the rest of the
meta data tags.

Write a short description (teaser) of the release after the meta data header
(i.e. after the second `---` line). Keep this short, i.e. one or two sentences
or two-three bullet points. You can use Markdown syntax here.

If you want to write a longer text about the release, put a line containing
only `<!-- TEASER_END -->` after the teaser and write the long description
after it. Your release will then be displayed with a "Read more..." link after
the teaser, which leads to a separate page with the full text. This feature
is only meant for projects, which do not have release notes or a changelog
on their own web site. All others should just fill in the `link` URL in the
meta data.

Save the file and rebuild and check the page as described above under
"Build Instructions".


### Example release post source

```
---
author: Cody Monk
category: ''
date: 2020-01-15 18:21:03 UTC+01:00
description: ''
homepage: https://github.com/codemonkey/thingamob
link: https://github.com/codemonkey/thingamob/blob/master/CHANGELOG.md
repository: https://github.com/codemonkey/thingamob.git
slug: thingamob-101
tags: ''
title: thingamob 1.0.1
type: text
---
Changes:

* More awesomeness
* Fix a tiny bug, which prevented the program from working at all

<!-- TEASER_END -->

Optional detailed release description here...
```


[Quarterly Release Pact]: https://libremusicproduction.com/dev/release/
[Nikola]: https://getnikola.com/
