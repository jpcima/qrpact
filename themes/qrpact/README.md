Quarterly Release Pact Website Theme
------------------------------------

This is a theme for the [Nikola] static site / blog generator.

It is used for the website at:

https://libremusicproduction.com/dev/release

Author: [Christopher Arndt](http://chrisarndt.de)


[Nikola]: https://getnikola.com/
