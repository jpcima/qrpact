---
author: Christopher Arndt
category: ''
date: 2020-01-19 19:06:28 UTC+01:00
description: ''
homepage: https://github.com/SpotlightKid/jack-matchmaker
link: https://github.com/SpotlightKid/jack-matchmaker/blob/master/CHANGELOG.rst
repository: https://github.com/SpotlightKid/jack-matchmaker.git
slug: jack-matchmaker-080
tags: ''
title: jack-matchmaker 0.8.0
type: text
---
Auto-connect JACK ports as they appear and match patterns on the command line or in a
file.

Added `--verbosity` option and systemd service, minor fixes.
