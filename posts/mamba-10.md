---
author: Hermann Meyer
category: ''
date: 2020-07-14 09:01:00 UTC+02:00
description: ''
homepage: https://github.com/brummer10/Mamba
link: https://github.com/brummer10/Mamba/releases/tag/v1.0
repository: https://github.com/brummer10/Mamba.git
slug: mamba-10
tags: ''
title: Mamba 1.0
type: text
---
A virtual on-screen MIDI keyboard for the Jack Audio Connection Kit with NSM support.



Initial release.