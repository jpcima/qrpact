---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/moony/
link: https://git.open-music-kontrollers.ch/lv2/moony.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/moony.lv2
slug: moonylv2-0340
tags: ''
title: moony.lv2 0.34.0
type: text
---
Write LV2 control port and event filters in Lua.

* Added embedded text browser for rapid manual lookups
* Added  support for GL double buffering
* Changed Lua version to 5.4.0
* Removed superfluous KXStudio extension definitions from turtle manifests
* Removed unneeded files