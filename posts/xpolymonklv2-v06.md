---
author: Hermann Meyer
category: ''
date: 2020-01-16 09:53:00 UTC+01:00
description: ''
homepage: https://github.com/brummer10/XPolyMonk.lv2
link: https://github.com/brummer10/XPolyMonk.lv2/releases/tag/v0.6
repository: https://github.com/brummer10/XPolyMonk.lv2.git
slug: xpolymonklv2-v06
tags: ''
title: XPolyMonk.lv2 v0.6
type: text
---
XPolyMonk is polyphonic version of [XMonk.lv2](https://github.com/brummer10/Xmonk.lv2) with full midi support.

First official release.
