---
author: Vladimir Sadovnikov
category: ''
date: 2020-04-18 11:41:00 UTC+02:00
description: ''
homepage: https://lsp-plug.in/
link: https://github.com/sadko4u/lsp-plugins/blob/master/CHANGELOG.txt
repository: https://github.com/sadko4u/lsp-plugins.git
slug: lsp-plugins-1119
tags: ''
title: LSP Plugins 1.1.19
type: text
---
A collection of multi-format open-source plugins.

* Updated XDG integration.
* Refactored `sse::fft` functions for better portability.
* Fixed bug in `asimd::hdotp` functions for AArch64 causing invalid output.
* Made LV2 and standalone JACK UI resizable.
* Fixed plugin sizing issues.
* Refactored and fixed de-/encoding of MIDI messages.
* Fixed silent MIDI output for JACK plugins.
* Ecluded profiling binaries from release build.
