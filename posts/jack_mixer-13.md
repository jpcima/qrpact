---
author: Frédéric Peters et al.
category: ''
date: 2020-07-16 22:20 UTC+02:00
description: ''
homepage: https://rdio.space/jackmixer/
link: https://github.com/jack-mixer/jack_mixer/blob/main/NEWS
repository: https://github.com/jack-mixer/jack_mixer.git
slug: jack_mixer-13
tags: ''
title: jack_mixer 13
type: text
---
A GTK+ audio mixer for JACK

* Added NSM support
* Added a pre/post fader send switches
* Ctrl- and double-click, scroll, and click-drag-anywhere
  fader behaviors
* Added MIDI 'Pick Up' behavior and manually setting MIDI CCs
* Store preferences in .ini file and per-session XML files instead of GConf
* ... and much more
