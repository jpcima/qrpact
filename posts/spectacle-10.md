---
author: Jean Pierre Cimalando
category: ''
date: 2020-04-16 14:52:25 UTC+02:00
description: 'A graphical spectral analyzer'
homepage: https://github.com/jpcima/spectacle
link: https://github.com/jpcima/spectacle/blob/v1.0/README.md
repository: https://github.com/jpcima/spectacle.git
slug: spectacle-10
tags: ''
title: Spectacle 1.0
type: text
---
Real-time spectral analyzer using STFT

- Display on logarithmic musical scale
- Control over latency and precision
- Zoom and smooth interpolation
- Identification of value at pointer and peaks
