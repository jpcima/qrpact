---
author: Sven Jaehnichen
category: ''
date: 2020-07-11 11:15:00 UTC+02:00
description: ''
homepage: https://github.com/sjaehn/BSchaffl
link: https://github.com/sjaehn/BSchaffl/releases/tag/0.2
repository: https://github.com/sjaehn/BSchaffl.git
slug: bschaffl-02
tags: ''
title: B.Schaffl 0.2
type: text
---
A pattern-controlled MIDI amp & stretch LV2 plugin.

New features:

* Randomize amp
* Randomize stretch
* Process amount for amp and stretch
* Shape editor
* Reset, undo and redo
