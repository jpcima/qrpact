---
author: Volker Fischer et al.
category: ''
date: 2020-07-19 08:47:00 UTC+02:00
description: ''
homepage: https://sourceforge.net/projects/llcon/
link: https://github.com/corrados/jamulus/blob/master/ChangeLog
repository: https://github.com/corrados/jamulus.git
slug: jamulus-359
tags: ''
title: Jamulus 3.5.9
type: text
---
Enables musicians to perform real-time jam sessions over the internet.

* New icon
* UI improvements for server and client
* Save and restore mixer state
* ... and more new features and bug fixes