---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/lv2lint/
link: https://gitlab.com/OpenMusicKontrollers/lv2lint/blob/master/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/lv2lint
slug: lv2lint-060
tags: ''
title: lv2lint 0.6.0
type: text
---
Check whether a given LV2 plugin is up to the specification.

* New checks
* Improve include_dir handling
* Option default changes
* Fixes.
