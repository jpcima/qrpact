---
author: Alexandros Theodotou
category: ''
date: 2020-01-14 20:25:00 UTC+01:00
description: ''
homepage: https://www.zrythm.org/
link: https://github.com/zrythm/zrythm/blob/master/CHANGELOG.md
repository: https://git.zrythm.org/cgit/
slug: zrythm-07367
tags: ''
title: Zrythm 0.7.367
type: text
---
Zrythm is a digital audio workstation designed to be featureful and
easy to use.

We believe that Zrythm is now in the late alpha stage where most
essential features are implemented and it's starting to be more stable,
and we are approaching beta.
