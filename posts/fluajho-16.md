---
author: Nils Hilbricht
category: ''
date: 2020-07-14 20:23:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/fluajho/
link: https://git.laborejo.org/lss/Fluajho/src/branch/master/CHANGELOG
repository: http://git.laborejo.org/lss/fluajho.git
slug: fluajho-16
tags: ''
title: Fluajho 1.6
type: text
---
*Fluajho* is a simple SF2 soundfont host/player for Linux.

* New GUI layout
* Do not auto connect under NSM
* Fix crashes and problems with recent Qt 5.15 release
* Quit under NSM works now
* Fix accidental transparency in Wayland
* Bug fixes, documentation update.