---
author: Nils Hilbricht
category: ''
date: 2020-04-15 01:37:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/patroneo/
link: https://git.laborejo.org/lss/Patroneo/src/branch/master/CHANGELOG
repository: https://git.laborejo.org/lss/patroneo.git
slug: patroneo-15
tags: ''
title: Patroneo 1.5
type: text
---
*Patroneo* is an easy to use, pattern based midi
sequencer.

* Remember looped measure number even when changing the tracks
* Better desktop program icons
* Provide unix manpages
* Bug fixes, documentation update
