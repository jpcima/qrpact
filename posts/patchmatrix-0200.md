---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lad/patchmatrix/
link: https://git.open-music-kontrollers.ch/lad/patchmatrix/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lad/patchmatrix
slug: patchmatrix-0200
tags: ''
title: patchmatrix 0.20.0
type: text
---
A graphical JACK patchbay combining a matrix-style and flow canvas interface.

* Added support for GL double buffering
* Removed support dynamic scaling
* Fixed crashes due to disabled X11 threads
