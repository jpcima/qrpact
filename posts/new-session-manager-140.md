---
author: Nils Hilbricht et al.
category: ''
date: 2020-07-14 20:44:00 UTC+02:00
description: ''
homepage: https://github.com/linuxaudio/new-session-manager
link: https://github.com/linuxaudio/new-session-manager/blob/master/CHANGELOG
repository: https://github.com/linuxaudio/new-session-manager.git
slug: new-session-manager-140
tags: ''
title: New Session Manager 1.4.0
type: text
---
A community, backwards-compatible version of "NON Session Manager".

* Add documentation and manpages
* Legacy-GUI look and feel overhaul
* `nsmd`: `NSM_API_VERSION_MINOR` bump 1.0 -> 1.1 and various fixes