---
author: Open Music Kontrollers
category: ''
date: 2020-01-15 23:35:28 UTC+01:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/shells_bells/
link: https://gitlab.com/OpenMusicKontrollers/shells_bells.lv2/blob/master/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/shells_bells.lv2
slug: shells_bellslv2-020
tags: ''
title: shells_bells.lv2 0.2.0
type: text
---
A just-for-fun LV2 plugin bundle. Sound the bell in the shell to send a MIDI note.

Initial release.
