---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/mephisto/
link: https://git.open-music-kontrollers.ch/lv2/mephisto.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/mephisto.lv2
slug: mephistolv2-080
tags: ''
title: mephisto.lv2 0.8.0
type: text
---
Write LV2 audio/CV instruments/filters directly in your host using the FAUST
DSP language.

* Fixed wrong note frequency at noteOn when pitch bending
* Deprecated release duration paramter
* Now builds with pugl master
* Use spinners instead of dials
* Adapt color theme autmatically  to terminal colors
* Slots in UI are counted from 0
