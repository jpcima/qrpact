---
author: Will J Godfrey
category: ''
date: 2020-01-18 16:20:00 UTC+01:00
description: ''
homepage: https://github.com/Yoshimi/yoshimi
link: https://github.com/Yoshimi/yoshimi/blob/master/Changelog
repository: https://github.com/Yoshimi/yoshimi.git
slug: yoshimi-170
tags: ''
title: Yoshimi 1.7.0
type: text
---
A sophisticated soft-synth originally forked from ZynAddSubFX

Instead of a few controls giving an immediate response,
there are only a few that don't :)