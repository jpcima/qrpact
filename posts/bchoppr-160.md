---
author: Sven Jaehnichen
category: ''
date: 2020-04-14 21:37:00 UTC+02:00
description: ''
homepage:
link: https://github.com/sjaehn/BChoppr/releases/tag/1.6.0
repository: https://github.com/sjaehn/BChoppr.git
slug: bchoppr-160
tags: ''
title: B.Choppr 1.6.0
type: text
---
An audio stream chopping LV2 plugin (and the successor of B.Slizr).

* Swing option
* Auto button: (Re-)set all markers to "auto" at once
* Visualize "auto" markers vs. "manual" markers
* Plugin description and video tutorial linked

