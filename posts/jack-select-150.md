---
author: Christopher Arndt
category: ''
date: 2020-01-19 23:13:02 UTC+01:00
description: ''
homepage: https://github.com/SpotlightKid/jack-select
link: https://github.com/SpotlightKid/jack-select/blob/master/CHANGELOG.rst
repository: https://github.com/SpotlightKid/jack-select.git
slug: jack-select-150
tags: ''
title: jack-select 1.5.0
type: text
---
A systray app to set the JACK configuration from QjackCtl presets via DBus

Added menu entries to control a2jmidid via D-BUS as well.
