---
author: Nils Hilbricht
category: ''
date: 2020-07-14 20:24:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/argodejo
link: https://git.laborejo.org/lss/Argodejo/src/branch/master/CHANGELOG
repository: http://git.laborejo.org/lss/argodejo.git
slug: argodejo-01
tags: ''
title: Argodejo 0.1
type: text
---
An NSM-compatible music production session manager GUI.

Initial "proof-of-concept" release.