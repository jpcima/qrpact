---
author: MusE Development Team
category: ''
date: 2020-07-18 12:50:00 UTC+02:00
description: ''
homepage: https://muse-sequencer.github.io/
link: https://github.com/muse-sequencer/muse/releases/tag/muse_3_1_1
repository: https://github.com/muse-sequencer/muse.git
slug: muse-311
tags: ''
title: MusE 3.1.1
type: text
---
A MIDI/Audio sequencer aiming to be a complete multitrack virtual studio for
Linux.

This is mostly a bugfix release following 3.1. A number of small and bigger
bugs have been squashed. See the
[release notes](https://github.com/muse-sequencer/muse/releases/tag/muse_3_1_1)
for more information.
