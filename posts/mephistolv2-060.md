---
author: Open Music Kontrollers
category: ''
date: 2020-01-15 23:35:03 UTC+01:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/mephisto/
link: https://gitlab.com/OpenMusicKontrollers/mephisto.lv2/blob/master/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/mephisto.lv2
slug: mephistolv2-060
tags: ''
title: mephisto.lv2 0.6.0
type: text
---
A Just-in-Time FAUST compiler embedded in an LV2 plugin

New UI, new features and fixes.
