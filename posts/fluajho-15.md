---
author: Nils Hilbricht
category: ''
date: 2020-04-15 01:37:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/fluajho/ject/CHANGELOG.md
link: https://git.laborejo.org/lss/Fluajho/src/branch/master/CHANGELOG
repository: http://git.laborejo.org/lss/fluajho.git
slug: fluajho-15
tags: ''
title: Fluajho 1.5
type: text
---
*Fluajho* is a simple SF2 soundfont host/player for Linux.

* Added convenience summing stereo-out jack ports.
* Show loaded sf2 as NSM label
* Better desktop program icons
* Provide unix manpages
* Bug fixes, documentation update.
