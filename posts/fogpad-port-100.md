---
author: Jean Pierre Cimalando
category: ''
date: 2020-01-15 18:36:58 UTC+01:00
description: ''
homepage: https://github.com/linuxmao-org/fogpad/
link: https://github.com/linuxmao-org/fogpad#changelog
repository: https://github.com/linuxmao-org/fogpad.git
slug: fogpad-port-100
tags: ''
title: Fogpad port 1.0.0
type: text
---
A reverb effect in which the reflections can be frozen, filtered, pitch shifted and ultimately
disintegrated.

Initial stable version of the (unofficial) Fogpad port.
