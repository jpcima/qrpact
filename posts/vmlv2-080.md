---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/vm/
link: https://git.open-music-kontrollers.ch/lv2/vm.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/vm.lv2
slug: vmlv2-080
tags: ''
title: vm.lv2 0.8.0
type: text
---
A programmable virtual machine LV2 plugin.

* Now builds with pugl master