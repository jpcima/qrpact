---
author: Stefan Westerfeld
category: ''
date: 2020-04-18 01:50:00 UTC+02:00
description: ''
homepage: https://uplex.de/audiowmark/
link: https://code.uplex.de/stefan/audiowmark/blob/master/NEWS
repository: https://code.uplex.de/stefan/audiowmark
slug: audiowmark-041
tags: ''
title: audiowmark 0.4.1
type: text
---
An Open Source (GPL) solution for audio watermarking.

Initial public release.