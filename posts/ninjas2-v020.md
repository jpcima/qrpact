---
author: Rob van den Berg
category: ''
date: 2020-01-17 13:06:00 UTC+01:00
description: ''
homepage: https://github.com/rghvdberg/ninjas2
link: https://github.com/rghvdberg/ninjas2/releases/tag/v0.2.0
repository: https://github.com/rghvdberg/ninjas2.git
slug: ninjas2-v020
tags: ''
title: Ninjas2 v0.2.0
type: text
---
A sample slicer audio LV2 and VST2 plugin and JACK stand-alone application.

Redesigned interface and improved on-screen keyboard.
