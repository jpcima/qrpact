---
author: Stefan Westerfeld
category: ''
date: 2020-01-12 18:34:00 UTC+01:00
description: ''
homepage:  https://github.com/swesterfeld/liquidsfz
link: https://github.com/swesterfeld/liquidsfz/blob/master/NEWS
repository: https://github.com/swesterfeld/liquidsfz.git
slug: liquidsfz-020
tags: ''
title: liquidsfz 0.2.0
type: text
---
The main goal of liquidsfz is to provide an SFZ sampler implementation
library that is easy to integrate into other projects. A standalone jack
client and a LV2 plugin is also available.

liquidsfz is implemented in C++ and licensed under the GNU LGPL version
2.1 or later.
