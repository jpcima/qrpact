---
author: Will J Godfrey
category: ''
date: 2020-04-20 01:44:00 UTC+02:00
description: ''
homepage: http://yoshimi.github.io/
link: https://github.com/Yoshimi/yoshimi/blob/master/Changelog.md
repository: https://github.com/Yoshimi/yoshimi.git
slug: yoshimi-171
tags: ''
title: Yoshimi 1.7.1
type: text
---
A sophisticated soft-synth originally forked from ZynAddSubFX

* Global 'Pan Law' setting
* Channel and Key aftertouch configuration dialog
* 'Hyperbolic Secant' waveform shape for AddSynth and PadSynth
* Channel 'Solo' setting
* More performant legato handling
* Instrument bank handling improvements
* Improved GUI window positioning and position recovery
* Significant refactoring of critical areas of the code