---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/midi_matrix/
link: https://git.open-music-kontrollers.ch/lv2/midi_matrix.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/midi_matrix.lv2
slug: midi_matrixlv2-0240
tags: ''
title: midi_matrix.lv2 0.24.0
type: text
---
MIDI channel filter, multiplexer and router LV2 plugin.

* Now builds with pugl master
