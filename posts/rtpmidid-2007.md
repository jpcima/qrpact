---
author: David Moreno Montero
category: ''
date: 2020-07-11 17:13:00 UTC+02:00
description: ''
homepage: https://github.com/davidmoreno/rtpmidid
link: https://github.com/davidmoreno/rtpmidid/releases/tag/v20.07
repository: https://github.com/davidmoreno/rtpmidid.git
slug: rtpmidid-2007
tags: ''
title: rtpmidid 20.07
type: text
---
Allows to share ALSA MIDI devices on the network via RTP MIDI and import
network-shared RTP MIDI devices.

* Compatibility with macOS/iOS RTP MIDI implementation
* Basic CLI interface
* Separate, LGPL2.1 licensed library for the rtpmidi protocol
* Improved stability.
