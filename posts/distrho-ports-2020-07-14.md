---
author: Filipe Coelho
category: ''
date: 2020-07-14 15:12:0 UTC+02:00
description: ''
homepage: https://distrho.sourceforge.io/ports
link: https://github.com/DISTRHO/DISTRHO-Ports/releases/tag/2020-07-14
repository: https://github.com/DISTRHO/DISTRHO-Ports.git
slug: distrho-ports-2020-07-14
tags: ''
title: DISTRHO-Ports 2020-07-14
type: text
---
Third-party audio plugins ported to Linux and LV2.

* Added **HiReSam**, **ReFine** and **Temper** plugin ports
* Fixed build for ARM NEON target
* Fixed LV2 UI notifications happening in non-GUI thread
* Fixed LV2 meta-data to pass lv2lint errors
* Updated JUCE
* Switched build system from premake3 to meson
