---
author: Jean Pierre Cimalando
category: ''
date: 2020-01-15 18:25:39 UTC+01:00
description: ''
homepage: https://github.com/linuxmao-org/VL1-emulator
link: https://github.com/linuxmao-org/VL1-emulator#changelog
repository: https://github.com/linuxmao-org/VL1-emulator
slug: vl1-emulator-1100
tags: ''
title: VL1 Emulator 1.1.0.0
type: text
---
An emulator of Casio VL-Tone VL1, based on source code by PolyValens

This version is a marker of a new release series, which is a cross-platform modification of the
original VSTi source code. The functionality is mostly but not exactly identical to the original,
and there remain some minor items to be implemented.
