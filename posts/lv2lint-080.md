---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/lv2lint/
link: https://gitlab.com/OpenMusicKontrollers/lv2lint/blob/master/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/lv2lint
slug: lv2lint-080
tags: ''
title: lv2lint 0.8.0
type: text
---
Check whether a given LV2 plugin is up to the specification.

* Added support for whitelisting various features
* Added lots of new checks
* Added support for `state:freePath` feature
* Updated minimum LV2 version to 1.18.0
* Fixed various memleaks
