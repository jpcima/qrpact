---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/midi_matrix/
link: https://git.open-music-kontrollers.ch/lv2/midi_matrix.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/midi_matrix.lv2
slug: midi_matrixlv2-0260
tags: ''
title: midi_matrix.lv2 0.26.0
type: text
---
MIDI channel filter, multiplexer and router LV2 plugin.

* Added support for GL double buffering
* Removed superfluous KXStudio extension definitions from turtle manifests