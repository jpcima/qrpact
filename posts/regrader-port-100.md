---
author: Jean Pierre Cimalando
category: ''
date: 2020-01-15 18:59:20 UTC+01:00
description: ''
homepage: https://github.com/linuxmao-org/regrader/
link: https://github.com/linuxmao-org/regrader#changelog
repository: https://github.com/linuxmao-org/regrader.git
slug: regrader-port-100
tags: ''
title: Regrader port 1.0.0
type: text
---
A delay effect where the repeats degrade in resolution

This is an initial version of the (unofficial) Regrader port.
