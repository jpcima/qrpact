---
author: Filipe Coelho
category: ''
date: 2020-04-13 07:33:00 UTC+02:00
description: ''
homepage: https://kx.studio/Applications:Carla
link: https://kx.studio/News/?action=view&url=carla-21-is-here
repository: https://github.com/falkTX/Carla.git
slug: carla-21
tags: ''
title: Carla 2.1
type: text
---
A fully-featured modular audio plugin host

* Better CV Support
* High-DPI support (work in progress)
* VST2 plugin for macOS and Windows, plus exposed parameters
* Refreshed add-plugin dialog and favorite plugins
* Single-page and grouped plugin parameters
* UI changes
