---
author: Damien Zammit
category: ''
date: 2020-01-16 09:34:29 UTC+01:00
description: ''
homepage: http://www.zamaudio.com/?p=976
link: https://github.com/zamaudio/zam-plugins/blob/master/changelog
repository: https://github.com/zamaudio/zam-plugins.git
slug: zam-plugins-312
tags: ''
title: zam-plugins 3.12
type: text
---
A collection of LADSPA/LV2/VST/JACK audio plugins for high-quality processing

Bugfixes and DSP improvements.
