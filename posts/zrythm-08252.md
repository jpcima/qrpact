---
author: Alexandros Theodotou
category: ''
date: 2020-04-10 23:48:31 UTC+02:00
description: ''
homepage: https://www.zrythm.org
link: https://git.zrythm.org/cgit/zrythm/tree/CHANGELOG.md
repository: https://git.zrythm.org/cgit/
slug: zrythm-08252
tags: ''
title: Zrythm 0.8.252
type: text
---
Zrythm is a digital audio workstation designed to be featureful and
easy to use.

It is in the late alpha stage and we are implementing the final few
features left before we stabilize the project format and enter the beta
phase.
