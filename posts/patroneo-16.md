---
author: Nils Hilbricht
category: ''
date: 2020-07-14 20:23:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/patroneo/
link: https://git.laborejo.org/lss/Patroneo/src/branch/master/CHANGELOG
repository: https://git.laborejo.org/lss/patroneo.git
slug: patroneo-16
tags: ''
title: Patroneo 1.6
type: text
---
*Patroneo* is an easy to use, pattern based midi sequencer.

* Fix crashes and problems with recent Qt 5.15 release
* Quit under NSM works now
* Fix accidental transparency in Wayland
* Bug fixes, documentation update.